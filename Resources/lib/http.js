/*
 * HTTP network module
 */

function isOnline() {
  var online = Ti.Network.online;
  return online;
}

/**
 * Get JSON from specified URL.
 * 
 * @param {String} url JSON source URL
 * @param {Function} callback(err, json)
 */
exports.get = function(url, callback) {  
  if (isOnline()) {
    var client = Ti.Network.createHTTPClient();
    client.open('GET', url);
    console.log('GET from %s', url);
    
    client.onload = function() {
      console.debug(this.getResponseHeader('Set-Cookie'));
      console.debug(this.responseText);
      callback(null, this.responseText);
    };
    
    client.onerror = function(err) {
      callback(new Error(L('err_network')), this.status);
    };
    
    client.send();
  } else {
    callback(new Error(L('err_offline')));
  }
};

/**
 * Get JSON from specified URL.
 * 
 * @param {String} url JSON source URL
 * @param {Function} callback(err, json)
 */
exports.getJSON = function(url, callback) {  
  if (isOnline()) {
    var client = Ti.Network.createHTTPClient();
    client.open('GET', url);
    console.log('GET JSON from %s', url);
    
    client.onload = function() {
      console.debug('getJSON: %s', this.getResponseHeader('Set-Cookie'));
      console.debug('getJSON: %s', this.responseText);
      var json = JSON.parse(this.responseText);
      callback(null, json);
    };
    
    client.onerror = function(err) {
      callback(new Error(L('err_network')), this.status);
    };
    
    client.send();
  } else {
    callback(new Error(L('err_offline')));
  }
};

/**
 * POST JSON to specified URL.
 * 
 * @param {String} url JSON source URL
 * @param {Object} params POST data
 * @param {Function} callback(err, json)
 */
exports.post = function(url, params, callback) {
  if (isOnline()) {
    var client = Ti.Network.createHTTPClient();
    client.open('POST', url);
    client.setRequestHeader('Content-Type', 'application/json');
    console.log('POST to %s', url);
    console.log(params);
    
    client.onload = function() {
      console.debug('post: %s', this.getResponseHeader('Set-Cookie'));
      console.debug('post: %s', this.responseText);
      callback(null, this.responseText);
    };
    
    client.onerror = function(err) {
      callback(new Error(L('err_network')), this.status);
    };
    
    client.send(params);
  } else {
    callback(new Error(L('err_offline')));
  }
}