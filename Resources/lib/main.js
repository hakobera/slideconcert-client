/*
 * Main module
 */

var auth = require('lib/auth'),
    api = require('lib/api'),
    style = require('ui/style')('tabs'),
    slideListWindow = require('ui/slideListWindow'),
    settingWindow = require('ui/settingWindow'),
    loginWindow = require('ui/loginWindow');

exports.run = function() {
  function next(err, data) {
    if (err) {
      if (data && data === 401) {
        console.debug(data);
        api.login(function(url) {
          var w = loginWindow.create({
            url: url
          });
          w.addEventListener('close', function(event) {
            auth.check(next);
          });  
          w.open();
        });
      } else {
        alert(err.message);
        auth.check(next);
      }
    } else {
      open();
    }
  }  
  auth.check(next);
};

function open() {
  var tabGroup = Ti.UI.createTabGroup();
  
  createSlideListTab(function(listTab) {
    tabGroup.addTab(listTab);
  });
  
  createSettingTab(function(settingTab) {
    tabGroup.addTab(settingTab);
  });
   
  tabGroup.open();
}

function createSlideListTab(callback) {
  var win = slideListWindow.create(),
      tab = Ti.UI.createTab(style.listTab({
        window: win
      }));
  callback(tab);   
}

function createSettingTab(callback) {
  var win = settingWindow.create(),
      tab = Ti.UI.createTab(style.settingTab({
        window: win
      }));
  callback(tab);   
}

