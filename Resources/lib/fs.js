function nop() {};

exports.writeFile = function(filename, data, callback) {
  var file = Ti.Filesystem.getFile(filename);
  callback = callback ? callback : nop;

  try {
    file.write(data);
    callback(null);
  } catch (e) {
    callback(e);
  }
};

exports.readFile = function(filename, callback) {
  var file = Ti.Filesystem.getFile(filename);
  callback = callback ? callback : nop;

  if (file.exists()) {
    callback(null, file.read());  
  } else {
    callback(new Error('file not found: ' + filename));
  }
};