/*
 * Authentication module
 */

var config = require('config'),
    api = require('lib/api'),
    db = require('lib/db');
    
var check = exports.check = function(callback) {
  var url = config.get('apiHost') + '/loggedIn';
  
  db.get('cert', function(err, cert) {
    var params = {};
    if (!err) {
      params.id = cert.id;
      params.token = cert.token;
    }
        
    api.loggedIn(params, function(err, data) {
      callback(err, data);
    });    
  });
};