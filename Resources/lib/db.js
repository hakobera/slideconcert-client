/*
 * Simple JSON database using file system.
 */

var fs = require('lib/fs');

var ROOT_DIR = Ti.Filesystem.applicationDataDirectory + '../Library/Caches/';

/**
 * Store data related by key
 * 
 * @param {String} key Key of data
 * @param {Object} data data to set
 * @param {Function} callback Call when data is saved or failed (optional)
 */
exports.set = function(key, data, callback) {
  var file = ROOT_DIR + key,
      content = JSON.stringify(data);
      
  console.debug('set %s: %s', key, content);
  fs.writeFile(file, content, callback);
};

/**
 * Get data releated by key.
 * 
 * @param {String} key Key of data
 * @param {Function} callback Call when data is saved or failed (optional)
 */
exports.get = function(key, callback) {
  var file = ROOT_DIR + key;

  fs.readFile(file, function(err, buffer) {
    if (err) return callback(err);
    
    var content = buffer.toString(),
        data = JSON.parse(content);
        
    console.debug('get %s: %s', key, data);
    callback(null, data);    
  });
};
