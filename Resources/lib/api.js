var config = require('config'),
    http = require('lib/http'),
    db = require('lib/db');
    
Ti.include('vendor/sha1.js');
    
var ACCESS_SECRET = 'KL397keica3jI38kdive+kd4jkILQEIXO';

function host(path) {
  return config.get('apiHost') + path;
};

function makeSign(id, date, token) {
  var sign = hex_hmac_sha1(ACCESS_SECRET, id + ':' + date + ':' + token);
  return sign;
}

function makeSingedQuery(callback) {
  db.get('cert', function(err, cert) {
    if (err) return callback('');
    
    var id = cert.id,
        token = cert.token,
        date = Date.now(),
        sign = makeSign(id, date, token),
        query = 'id=' + id + '&date=' + date + '&sign=' + sign;
    
    console.debug('singedQuery: %s', query);
    callback(query);
  });
}

/**
 * Login check API
 */
exports.loggedIn = function(options, callback) {
  var uri = host('/loggedIn'),
      data = {};
  
  if (options.id && options.token) {
    data.id = options.id;
    data.date = Date.now();
    data.sign = makeSign(options.id, data.date, options.token);
  }
  
  http.post(uri, data, callback);
};

/**
 * Login URL
 */
exports.login = function(callback) {
  console.log('login');
  callback(host('/auth'));
};

/**
 * FindSliedByUser
 */
exports.findSlideByUser = function(options, callback) {
  var url = host('/api/users/' + options.user + '/slides');
  makeSingedQuery(function(query) {
    var uri = String.format('%s?%s', url, query);
    console.debug('findSlideByUser: %s', uri);
    http.getJSON(uri, callback);
  });
};

/**
 * Check and build controller API
 */
exports.controller = function(options, callback) {
  var url = host('/control/open/' + options.user + '/' + options.docId);
  makeSingedQuery(function(query) {
    var uri = String.format('%s?%s&numSlides=%s', url, query, options.numSlides);
    console.debug('cotroller: %s', uri);
    callback(null, uri);
  });
};

