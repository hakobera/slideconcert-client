var DEVELOPMENT = exports.DEVELOPMENT = 'development';
var PRODUCTION = exports.PRODUCTION = 'production';

/**
 * Get configuration value for key
 * 
 * @return {Object} value for key
 */
exports.get = function(key) {
  return config[env][key];
};

/*
 * Select environment you use.
 */

//var env = DEVELOPMENT;
var env = PRODUCTION;

/*
 * Configuration for each environment.
 */
var config = {}

config[DEVELOPMENT] = {
  apiHost: 'http://localhost:3000'
};

config[PRODUCTION] = {
  apiHost: 'http://www.slideconcert.com' 
};

console.log(config[env]);