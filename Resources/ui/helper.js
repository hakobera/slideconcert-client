var style = require('ui/style')('common');

/**
 * Create FlexSpace
 *
 * @return {Ti.UI.Button}
 */
exports.createFlexSpace = function() {
  var flexSpace = Ti.UI.createButton({
    systemButton : Ti.UI.iPhone.SystemButton.FLEXIBLE_SPACE
  });
  return flexSpace;
};
/**
 * Create activity indicator
 */
exports.createActivityIndicator = function() {
  return Ti.UI.createActivityIndicator(style.activityIndicator());
};
/**
 * Create retry button
 *
 * @param {Function} callback callback(button)
 */
exports.createRetryButton = function(callback) {
  var button = Ti.UI.createButton(style.retryButton());
  callback(button);
  return button;
};
/**
 * Create login button
 *
 * @param {Function} callback callback(button)
 */
exports.createLoginButton = function(callback) {
  var button = Ti.UI.createButton(style.loginButton());
  callback(button);
  return button;
};
/**
 * Create pull to refresh header
 */
exports.createRefreshHeader = function(tableView, onReload) {
  var border = Ti.UI.createView({
    backgroundColor : "#576c89",
    height : 2,
    bottom : 0
  });

  var tableHeader = Ti.UI.createView({
    backgroundColor : "#e2e7ed",
    width : 320,
    height : 60
  });

  // fake it til ya make it..  create a 2 pixel
  // bottom border
  tableHeader.add(border);

  var arrow = Ti.UI.createView({
    backgroundImage : "images/whiteArrow.png",
    width : 23,
    height : 60,
    bottom : 10,
    left : 20
  });

  var statusLabel = Ti.UI.createLabel({
    text : "Pull to reload",
    left : 55,
    width : 200,
    bottom : 30,
    height : "auto",
    color : "#576c89",
    textAlign : "center",
    font : {
      fontSize : 13,
      fontWeight : "bold"
    },
    shadowColor : "#999",
    shadowOffset : {
      x : 0,
      y : 1
    }
  });

  var lastUpdatedLabel = Ti.UI.createLabel({
    text : "Last Updated: " + formatDate(),
    left : 55,
    width : 200,
    bottom : 15,
    height : "auto",
    color : "#576c89",
    textAlign : "center",
    font : {
      fontSize : 12
    },
    shadowColor : "#999",
    shadowOffset : {
      x : 0,
      y : 1
    }
  });

  var actInd = Titanium.UI.createActivityIndicator({
    left : 20,
    bottom : 13,
    width : 30,
    height : 30
  });

  tableHeader.add(arrow);
  tableHeader.add(statusLabel);
  tableHeader.add(lastUpdatedLabel);
  tableHeader.add(actInd);

  tableView.headerPullView = tableHeader;

  var pulling = false;
  var reloading = false;

  function beginReloading() {
    onReload(tableView, endReloading);
  }

  function endReloading() {
    // when you're done, just reset
    tableView.setContentInsets({
      top : 0
    }, {
      animated : true
    });
    reloading = false;
    lastUpdatedLabel.text = "Last Updated: " + formatDate();
    statusLabel.text = "Pull down to refresh...";
    actInd.hide();
    arrow.show();
  }


  tableView.addEventListener('scroll', function(e) {
    var offset = e.contentOffset.y;
    if(offset <= -65.0 && !pulling) {
      var t = Ti.UI.create2DMatrix();
      t = t.rotate(-180);
      pulling = true;
      arrow.animate({
        transform : t,
        duration : 180
      });
      statusLabel.text = "Release to refresh...";
    } else if(pulling && offset > -65.0 && offset < 0) {
      pulling = false;
      var t = Ti.UI.create2DMatrix();
      arrow.animate({
        transform : t,
        duration : 180
      });
      statusLabel.text = "Pull down to refresh...";
    }
  });

  tableView.addEventListener('scrollEnd', function(e) {
    if(pulling && !reloading && e.contentOffset.y <= -65.0) {
      reloading = true;
      pulling = false;
      arrow.hide();
      actInd.show();
      statusLabel.text = "Reloading...";
      tableView.setContentInsets({
        top : 60
      }, {
        animated : true
      });
      arrow.transform = Ti.UI.create2DMatrix();
      beginReloading();
    }
  });
};

function formatDate() {
  var date = new Date();
  var datestr = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
  if(date.getHours() >= 12) {
    datestr += ' ' + (date.getHours() === 12 ? date.getHours() : date.getHours() - 12) + ':' + date.getMinutes() + ' PM';
  } else {
    datestr += ' ' + date.getHours() + ':' + date.getMinutes() + ' AM';
  }
  return datestr;
}