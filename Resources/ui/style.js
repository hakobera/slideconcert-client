/**
 * Merge object properties and return as new object.
 * If base and options has same key, overrided by options value.
 * 
 * @param {Object} base Base property object
 * @param {Object} options Option property object
 * @return {Object} Merged object
 */
function merge(base, options) {
  var result = {}, k;
  for (k in base) {
    if (base.hasOwnProperty(k)) {
      result[k] = base[k];
    }
  }
  for (k in options) {
    if (options.hasOwnProperty(k)) {
      result[k] = options[k];
    }
  }
  return result; 
}

/**
 * Create style function.
 * 
 * @return {Function} function(options)
 */
function S(base) {
  return function(options) {
    if (arguments.length === 0) {
      return base;
    } else {
      return merge(base, options);
    }
  }
}

var styles = {};

module.exports = function(name) {
  var style = styles[name];
  if (!style) {
    throw new Error('style not found: ' + name );
  }  
  
  var map = {};
  Object.keys(style).forEach(function(key) {
    map[key] = S(style[key]);
  });
  return map;
};

/**
 * Tabs
 */
styles.tabs = {
  listTab: {
    title: 'Slides',
    icon: 'images/dark_list.png'
  },
  
  settingTab: {
    title: 'Setting',
    icon: 'images/dark_gear.png'
  }
};

/**
 * Common UI style.
 */
styles.common = {
  activityIndicator: {
    width: '100%',
    message: L('title_loading'),
    font: { fontFamily: 'Helvetica Neue', fontSize: 16, fontWeight: 'bold' },
    color: '#ccc',
    style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN
  },
  
  retryButton: {
    title: L('retry_button'),
    width: 280,
    height: 50,
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 10      
  },
  
  loginButton: {
    title: L('login_button'),
    width: 280,
    height: 50,
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 10      
  }
};

/**
 * Styles for loginWindow
 */
styles.loginWindow = {
  window: {
    backgroundColor: '#eee'
  }
}

/**
 * Styles for SlideListWindow
 */
styles.slideListWindow = {
  window: {
    title: L('title_slide_list_window')
  },
  
  tableView: {
    
  },
  
  tableRow: {
    height: 80,
    layout: 'absolute' 
  },
    
  thumbnailImage: {
    width: 70,
    height: 70,
    top: 5,
    left: 5
  },
    
  dateLabel: {
    width: 210,
    height: 12,
    top: 5,
    left: 80,
    font: { fontSize: 10 },
    color: '#666'
  },
  
  slideTitleLabel: {
    width: 210,
    height: 28,
    top: 18,
    left: 80,
    font: { fontSize: 12, fontWeight: 'bold' },
    color: '#cf7823'
  },
    
  slideDescriptionLabel: {
    width: 210,
    height: 30,
    top: 45,
    left: 80,
    font: { fontSize: 12 },
    fontWeight: 'bold',
    color: '#333'
  }
};

/**
 * Styles for slideControlWindow
 */
styles.slideControlWindow = {
  window: {
  },
  
  toolbar: {
    bottom: 0,
    borderTop: true,
    borderBottom: false
  },
  
  closeButton: {
    labels: [ L('close_button') ],
    width: 50,
    font: { fontSize: 12 },
    backgroundColor: '#369'
  },

  titleView: {
    top: 0,
    height: 50,
    backgroundColor: '#000'
  },

  titleLabel: {
    width: 300,
    height: 40,
    left: 10,
    color: '#fff',
    font: { fontSize: 14 },
  },
  
  slideView: {
    height: 206,
    top: 210,
    backgroundColor: '#fff'
  },
  
  thumbnailImage: {
    height: 120,
    top: 10
  },
  
  slideDescriptionLabel: {
    top: 110,
    width: 300,
    color: '#333',
    font: { fontSize: 12 } 
  },
  
  controlView: {
    top: 50,
    height: 160
  }
};

/**
 * Styles for settingWindow
 */
styles.settingWindow = {
  window: {
    title: L('title_setting_window') 
  },
  
  tableView: {
    style: Ti.UI.iPhone.TableViewStyle.GROUPED
  },
  
  tableRow: {
    height: 50
  },
  
  tableRowLabel: {
    left: 10,
    width: 80,
    font: { fontSize: 12, fontWeight: 'bold' }
  },
  
  tableRowTextField: {
    color: '#336699',
    height: 35,
    top: 10,
    left: 90,
    width: 200,
    borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE
  }
};
