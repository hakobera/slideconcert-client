var db = require('lib/db'),
    helper = require('ui/helper'),
    style = require('ui/style')('loginWindow');

function initialize(win, url) {
  helper.createLoginButton(function(loginButton) {
    loginButton.addEventListener('click', function(event) {
      var loginWin = Ti.UI.createWindow({
        modal: true,
        fullscreen: true,
        title: L('login_button')
      });
      
      var webView = Ti.UI.createWebView({
        url: url
      });
      webView.addEventListener('load', function(event) {
        console.debug('webView#load %s', event.url);
        try {
          var title = webView.evalJS("document.title");
          if (title === 'loginSuccess') {
            var user = {
              id: webView.evalJS("document.getElementById('id').innerText"),
              name: webView.evalJS("document.getElementById('name').innerText"),
              token: webView.evalJS("document.getElementById('token').innerText")
            };
            db.set('cert', user);
            loginWin.close();
            win.close();
          } else if (title === 'loginCancel') {
            loginWin.close();
          }
        } catch (e) {
          alert(e);
        }
      });
      loginWin.add(webView);
      loginWin.open();
    });    
    win.add(loginButton);
  });
}

exports.create = function(options) {
  var win = Ti.UI.createWindow(style.window());
  initialize(win, options.url);
  return win;
};

