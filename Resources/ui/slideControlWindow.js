var config = require('config'),
    api = require('lib/api'),
    style = require('ui/style')('slideControlWindow'),
    helper = require('ui/helper'),
    twitter = require('de.marcelpociot.twitter');
    
/**
 * Create slide control window.
 */
exports.create = function(options) {
  var win = Ti.UI.createWindow(style.window());
  initialize(win, options);
  return win;  
};

function initialize(win, options) {
  var slide = options.slide,
      user = options.user;
  
  createTitle(slide, function(error, title) {
    win.add(title);
  })
  
  createToolbar(slide, user, function(err, toolbar) {
    win.add(toolbar);  
  });
  
  createSlideView(slide, function(err, slideView) {
    win.add(slideView);  
  });

  createControlWebView(slide, user, function(err, controlView) {
    win.add(controlView);
  });

}

function createTitle(slide, callback) {
  var view = Ti.UI.createView(style.titleView()),
      label = Ti.UI.createLabel(style.titleLabel({
        text: slide.Title
      }));
      
  view.add(label);
      
  callback(null, view);
}

function createToolbar(slide, user, callback) {
  var toolbar,
      closeButton = Ti.UI.createButtonBar(style.closeButton()),
      flexSpace =  helper.createFlexSpace(),
      actionButton = Ti.UI.createButton({ systemButton: Ti.UI.iPhone.SystemButton.ACTION });

  actionButton.addEventListener('click', function() {
    var dialog = Ti.UI.createOptionDialog({
      options: [ L('tweet_button'), L('safari_button'), L('cancel_button') ],
      cancel: 2
    });

    dialog.addEventListener('click', function(event) {
      switch (event.index) {
      case 0:
        twitter.tweet({
          message: slide.Title,
          urls: [ slide.URL, config.get('apiHost') + '/live/' + user ],
          succes: function(){
            //alert("Tweet successfully sent");
          },
          cancel: function(){
            //alert("User canceled tweet");
          },
          error: function(){
            alert("Unable to send tweet");
          }
        });
        break;
        
      case 1:
        Ti.Platform.openURL(slide.URL);
        break;
      }
    });

    dialog.show();
  });

  closeButton.addEventListener('click', function() {
    toolbar.getParent().close({
      transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
    });
  });

  toolbar = Ti.UI.iOS.createToolbar(style.toolbar({
    items: [ actionButton, flexSpace, closeButton ]
  }));
  
  callback(null, toolbar);
}

function createSlideView(slide, callback) {
  var slideView = Ti.UI.createView(style.slideView()),
      thumbnailImage, descriptionLabel;
      
  thumbnailImage = Ti.UI.createImageView(style.thumbnailImage({
    image: slide.ThumbnailURL
  }))
  slideView.add(thumbnailImage);
  
  if (typeof slide.Description === 'string') {
    var descriptionLabel = Ti.UI.createLabel(style.slideDescriptionLabel({ text: slide.Description }));
    slideView.add(descriptionLabel);
  }
  
  callback(null, slideView);
}

function createControlWebView(slide, user, callback) {
  var options = {
    user: user,
    docId: getDocId(slide),
    numSlides: slide.NumSlides
  };
  console.debug(options);
  api.controller(options, function(err, uri) {
    var webView = Ti.UI.createWebView(style.controlView({
      url: uri
    }));
    callback(null, webView);
  });
}

function getDocId(slide) {
  var url = slide.ThumbnailURL,
      i = url.lastIndexOf('/');
  
  return url.substring(i + 1, url.length - '-thumbnail'.length);
}
