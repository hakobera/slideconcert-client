var db = require('lib/db'),
    style = require('ui/style')('settingWindow');

/**
 * Create slide list window.
 */
exports.create = function() {
  var win = Ti.UI.createWindow(style.window());
  initialize(win);
  return win;
};
/**
 * Initialize window and other components.
 */
function initialize(win) {
  var tableView = createTableView();
  win.add(tableView);
}

function createTableView(data) {
  var data = [];

  addRow(L('setting_slideshare_username'), function(row, ssuser) {
    db.get('slideshare', function(err, data) {
      if (!err) {
        ssuser.value = data.username;
      }
    });
    
    ssuser.addEventListener('blur', function(event) {
      db.set('slideshare', { username: ssuser.value });
    });
    
    data.push(row);
  });

  data[0].header = 'slideshare';
  var tableView = Ti.UI.createTableView(style.tableView({
    data : data
  }));

  return tableView;
}

function addRow(labelText, callback) {
  var row = Ti.UI.createTableViewRow(style.tableRow());

  var label = Ti.UI.createLabel(style.tableRowLabel({
    text : labelText
  }));

  var textField = Ti.UI.createTextField(style.tableRowTextField());

  row.add(label);
  row.add(textField);
  row.selectionStyle = Ti.UI.iPhone.TableViewCellSelectionStyle.NONE;

  callback(row, textField);
}