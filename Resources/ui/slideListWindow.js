var config = require('config'),
    api = require('lib/api'),
    db = require('lib/db'),
    style = require('ui/style')('slideListWindow'),
    helper = require('ui/helper'),
    slideControlWindow = require('ui/slideControlWindow');

/**
 * Create slide list window.
 */
exports.create = function() {
  var win = Ti.UI.createWindow(style.window());
  initialize(win);
  return win;
};

/**
 * Initialize window and other components.
 */
function initialize(win) {
  var actInd = helper.createActivityIndicator();
  win.add(actInd);
  actInd.show();
  
  db.get('slideshare', function(err, ssuser) {
    if (err) {
      alert(L('setting_slidesare_user_not_set'));
      createView({}, function(view) {
        win.add(view);
      });      
    } else {
      var options = { user: ssuser.username };
    
      function next(err, data) {
        win.remove(actInd);
        if (err) {
          alert(err.message);
          helper.createRetryButton(function(retryButton) {
            win.add(retryButton);        
            retryButton.addEventListener('click', function(e) {
              win.remove(retryButton);
              win.add(actInd);
              actInd.show();
              api.findSlideByUser(options, next);
            });
          });
        } else {
          createView(data, function(view) {
            win.add(view);
          });      
        }
      }
      api.findSlideByUser(options, next);
    }
  });
}

/**
 * Create slide list table view
 * 
 * @param {Array} slides Array of slide infomation
 */
function createView(data, callback) {
  var rows = [],
      slides = data.Slideshow || [],
      tableView;
  
  if (slides.length === 0) {
    rows.push({ text: 'test' });  
  } else {
    slides.forEach(function(slide, i) {
      var row = createRow(slide, i);
      rows.push(row);
    });        
  }
    
  tableView = createTableView(rows);
  callback(tableView);
}

function createRow(slide, index) {
  var row, thunbnailImage, dateLabel, titleLabel, descLabel;

  row = Ti.UI.createTableViewRow(style.tableRow({
    slide: slide,
    index: index
  }));
              
  thunbnailImage = Ti.UI.createImageView(style.thumbnailImage({
    image: slide.ThumbnailSmallURL
  }));
  row.add(thunbnailImage);

  dateLabel = Ti.UI.createLabel(style.dateLabel({
    text: slide.Created
  }));
  row.add(dateLabel);
  
  titleLabel = Ti.UI.createLabel(style.slideTitleLabel({
    text: slide.Title
  }));
  row.add(titleLabel);
  
  if (typeof slide.Description === 'string') {
    descLabel = Ti.UI.createLabel(style.slideDescriptionLabel({
     text: slide.Description
    }));
    row.add(descLabel);
  }
  return row;
}

function createTableView(rows) {
  var tableView = Ti.UI.createTableView({
    data: rows
  });
  
  tableView.addEventListener('click', function(event) {
    var row = event.rowData;
    db.get('cert', function(err, cert) {
      if (err) return alert('Login error');
      var w = slideControlWindow.create({
                user: cert.name,
                slide: row.slide
              });
      w.open({ transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT });
    });
  });
  
  helper.createRefreshHeader(tableView, function(view, end) {
    view.data = [];
    
    db.get('slideshare', function(err, ssuser) {
      if (err) return end();
      
      api.findSlideByUser({ user: ssuser.username }, function(err, data) {
        if (err) return end();
        var slides = data.Slideshow || [];
        slides.forEach(function(slide, i) {
          var row = createRow(slide, i);
          view.appendRow(row);
        });
        end();
      });        
    });       
  });
  
  return tableView;
}