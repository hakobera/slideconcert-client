var console = console || {};

(function() {
  function message(arg) {
    var len = arg.length,
        param = [],
        msg = '',
        i, v;
    
    for (i = 0; i < len; i++) {
      v = arg[i];
      if (typeof v === 'object') {
        v = JSON.stringify(v); 
      }
      param.push(v);
    }
    
    if (len === 1) {
      msg = param[0];
    } else if (len > 1) {
      msg = String.format.apply(String, param);
    }
    
    return msg;
  }
  
  console.log = function() {
    var msg = message(arguments);
    Ti.API.info(msg);
  };
  
  console.debug = function() {
    var msg = message(arguments);
    Ti.API.debug(msg);
  };
  
  console.error = function() {
    var msg = message(arguments);
    Ti.API.error(msg);
  };

  console.warn = function() {
    var msg = message(arguments);
    Ti.API.warn(msg);
  };

})();
